<h1 align="center">Laravel-API</h1>

laravel passport 不支持 多表登录 所以自己写了一个

## 运行环境

- php >= 5.6
- composer
- laravel >= 5.1

## 特点

- 简答、易懂、快速
- 专门为 laravel 打造
- 使用自带的认证驱动模式
- 多表 认证

## 安装

```shell
$ composer require chencongbao/laravel-api
```

### 添加 ServiceProvider（optional. if laravel < 5.5）

```php
// laravel < 5.5
Bob\LaravelApi\ApiServiceProvider::class,
```


## 使用方法

1. 更改认证驱动

    在 config/auth.php 中，更改 `guards.api.driver` 为 **api**

2. 在 UserModel 中添加 trait
    
    在 `config(auth.provider.xx.model)` 的类中，添加 `use Bob\LaravelApi\Models\Traits\HasApiApps`

3. 运行数据迁移

    `php artian migrate`

4. 开始使用吧


### 添加 App 与 accessToken

```php
use Bob\LaravelApi\Api;

'users' 为您的不同的 provider

$access_token = Api::generateAccessToken(User::find(1), 'users');
```

#### 使用

- header 上进行认证

- provider 为您的认证provider 比如 admin 表的  provider 为admins 

    例如：
    `Authorization: Bearer 3857d7d56f4ffe1ab57b8a8f292b85fa`
    `provider:users`


### 服务端认证

只需要在增加 `'auth:api'` 的 middleware 即可，增加后，`$request->user()/$request->user` 即为认证用户

```php
use Illuminate\Http\Request;

Route::middleware('auth:api')->get('user', function(Request $request) {
    dd($request->user());
    // $request->user 获取 user_id
});
```


## 其它

### access_token 有效时间

access_token 有效时间默认为 长期有效，当然，您可以自由设置该值：

```php
// 在 AppServiceProvider 的 register 方法中增加

use Bob\LaravelApi\Api;

Api::$ttl = 7200;
// Api::setTtl(7200);
```


## 异常

- AccessTokenNotProvidedException
    
    当客户端没有提供 access_token 时，抛出 `AccessTokenNotProvidedException` 异常，如果不进行任何额外的操作，默认将返回 `{"code":1,"message":"AccessToken Is Not Provided"}`

- InvalidAccessTokenException
    
    当客户端提供了无效的 access_token 时，抛出 `InvalidAccessTokenException` 异常，如果不进行任何额外的操作，默认将返回 `{"code":2,"message":"AccessToken Is Invalid"}`

- AccessTokenExpiredException

    当客户端提供的 access_token 已过期时，抛出 `AccessTokenExpiredException` 异常，如果不进行任何额外的操作，默认将返回 `{"code":3,"message":"AccessToken Is Expired"}`

- GenerateAccessTokenException

    当服务端生成 access_token 失败时，抛出 `GenerateAccessTokenException` 异常，如果不进行任何额外的操作，默认将返回 `{"code":4,"message":"xxxx"}`


_以上异常如果不想使用默认的信息，均可自行捕获更改相关信息_

## License

MIT

