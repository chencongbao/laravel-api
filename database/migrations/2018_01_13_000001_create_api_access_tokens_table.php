<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_access_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('provider', 190);
            $table->string('access_token', 190);
            $table->dateTime('expired_at');
            $table->smallInteger('long_term')->default(0)->comment('是否長期有效1=是，0=否');
            $table->timestamps();

            $table->index('user_id');
            $table->unique('access_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_access_tokens');
    }
}
