<?php

namespace Bob\LaravelApi\Models;

use Illuminate\Database\Eloquent\Model;
use Bob\LaravelApi\Api;

class AccessToken extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'api_access_tokens';

    /**
     * The guarded attributes on the model.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expired_at'];

    /**
     * Get the user that the token belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(Api::$user, 'user_id');
    }
}
