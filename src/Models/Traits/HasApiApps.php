<?php
namespace Bob\LaravelApi\Models\Traits;

use Bob\LaravelApi\Models\AccessToken;

trait HasApiApps
{

    /**
     * Get all of the access tokens for the user.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function tokens()
    {
        return $this->hasMany(AccessToken::class, 'user_id');
    }
}
